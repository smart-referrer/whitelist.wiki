## Gathering the Data

To help gather the information necessary, please follow the following steps:

  1. Open the network analyser panel (*Ctrl*+*Shift*+*E*) on the affected site.
  2. Ensure that the **All** button next to the “garbage can” is selected.
  3. Perform whatever action is broken – if the entire page is affected then just reload (*F5*).
  4. Upload a screenshot of the entire result:
     * If parts of the content are missing, try making the panel as large as possible before taking a screenshot.
     * If the contents still don't fit into the panel after resizing it, take several screenshots (on per panel page) until all the available information has been captured, then upload all of the screenshots.

The final result should look similar to the following:

![screenshot_20171031_203708](https://user-images.githubusercontent.com/246386/32245194-46c45428-be7b-11e7-840d-758f20774af9.png)

## Helping find the right rules

If you want to help in finding the right rules to apply, please read on. Please do note however that the following is based on experience and knowledge of the underlying protocols, an may not work (or a slightly different set of steps may be required).

  1. Look for any rows that **do not** report their *Status* as any of:
      * `101` (WebSocket Request)
      * `200` (Request OK)
      * `301`, `302` or `303` (Various Redirects)
      * `304` (Not Modified)
      * In particular, the following *Status* values **are suspicious** (in order):
         * `403` (Unauthorized)
         * `400` (Bad Request)
         * `404` (File not Found)
      * Also all entries marked as *From Cache* can definitely be ignored.
      * If a row with a suspicious status value changes their status value to something else if you redo whatever action was broken while *Smart Refer* is disabled (just click to toolbar button) then this is a very strong indicator that you may have found the issue!
   2. If you click on any entry a panel will appear towards the right (Not usually required but may be useful!).
      * By scrolling down in the *Headers* tab (default) to the *Request Headers* section you can view the `Referer` value that was actually sent for the given request.
      * By switching to the *Response* tab you can view the response received from the sever:
         * If this is totally empty or contains something about **Unauthorized**, **Denied**, **Not Allowed**, … it usually means you're on the right track. If it talks about an **Unknown Source** or similar, even better.
   3. Once you have identified a likely culprint try adding an entry to the **Exceptions** list of *Smart Referer*'s settings:
       * Start with a source of `*` (any host) and the hostname of the suspicous entry as destination.
       * If this doesn't work try another suspicious entry instead.
       * If this does work try limiting the scope of the source:
         * Readd the rule with a source of `.<site>.<tld>` (for `www.qwant.com` this would be `.qwant.com`, for `guardian.co.uk` this would be `.guardian.co.uk`)
         * If this doesn't work switch to the broken page again, disable *Smart Referer*, reload and look for the `Referer` value as described in section 2. You'll end up with an URL: Take its hostname part and try adding a rule with that hostname as source and the same destination as before.

Once you have a source and destination host (or just one of the two) that fixes the problem please add them to your issue report. Thanks for the help!